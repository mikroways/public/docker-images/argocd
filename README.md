# Imagen de ArgoCD + Helm plugins + Helmfile

Tanto la imagen de ArgoCD [official](https://hub.docker.com/r/argoproj/argocd)
como la de [bitnami](https://hub.docker.com/r/bitnami/argo-cd/) soportan 
[Helm](https://helm.sh/) pero sus plugins se deben instalar a mano.

En esta imagen desde la official de ArgoCD, se instalan:

* [Helm Secrets](https://github.com/jkroepke/helm-secrets): plugin de Helm que
   permite ecriptar/desencriptar, ver y editar archivos de *secrets*.
* [Helm Git](https://github.com/aslafy-z/helm-git): permite empaquetar charts.
* [argo-cd-helmfile](https://github.com/travisghansen/argo-cd-helmfile): un 
   plugin de Argo para soportar [Helmfile](https://github.com/roboll/helmfile).
* [Sops](https://github.com/mozilla/sops) y 
   [Age](https://github.com/FiloSottile/age): requeridos por Helm Secrets.
